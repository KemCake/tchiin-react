/**
 * @jsx React.DOM
 */
var React = require('react');
var Parse = require('parse').Parse;

/* First Level components */
var Menu = require('./Menu.react');
var Footer = require('./Footer.react');

/* ROOTER */
var Router = require('react-router-component');
var Locations = Router.Locations;
var Location = Router.Location;
var NotFound = Router.NotFound;

/* PAGES COMPONENTS */
var Home = require('./Home/Home.react');
var Map = require('./Map/Map.react');
var Place = require('./Place/Place.react');
var Profile = require('./Profile/Profile.react');
var NewPlace = require('./NewPlace/NewPlace.react');

var App = React.createClass({
  
  getInitialState: function() {
    var path = window.location.pathname+window.location.hash+window.location.search;
    path = path.replace('.html?','/');
    path = path.replace('.html','');
    return {path: path};
  },
  componentWillMount: function() {
    console.log(this.state.path);
  },


  /**
   * @return {object}
   */
  render: function() {    
  	return (
        <div className="app">
          <div className="main-container">
            <Locations path={this.state.path}>
              <Location path="/" handler={Home} />
              
              <Location path="/map" handler={Map} />
              <Location path="/map/:params" handler={Map} />
              
              <Location path="/place/:placeName" handler={Place} />
              
              <Location path="/newplace" handler={NewPlace} />
              <Location path="/newplace/:place" handler={NewPlace} />
              
              <Location path="/profile/:profileName" handler={Profile} />
              <NotFound handler={Home} />
            </Locations>
          </div>
          {/*<Footer/>*/}     
        </div>
  	);
  },


});

module.exports = App;
