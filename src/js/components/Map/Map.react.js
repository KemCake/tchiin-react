/**
 * @jsx React.DOM
 */
 
var $ = require('jquery');
require('bsmultiselect');
require('infoBox');

var Parse = require('parse').Parse;
var React = require('react');
var ReactPropTypes = React.PropTypes;
var Menu = require('../Menu.react');
var PlaceStore = require('../../stores/PlaceStore');

var Router = require('react-router-component');
var Link = require('react-router-component').Link;

/* var PlaceListPagination = require('./PlaceListPagination.react'); */
var PlaceListItem = require('./PlaceListItem.react');
var PlaceInfoBox = require('./PlaceInfoBox.react');

var _this;
var map;
var markersArray = [];
var locationMarker;
var geocoder;
var allPlaces;
var visiblePlaces;
var infoBox;

var Map = React.createClass({

  mixins:[Router.NavigatableMixin],
  
  getInitialState: function() {
    return {
      placeListItems:[],
      categories:[],
      tags:[]
    };
  },
  
  componentDidMount: function() {
		_this = this;
		
    window.infoBoxClick = this.infoBoxClick;
		/* $(function(){ */
  
  	      	// load Google Map          
  	    var latlng = new google.maps.LatLng(44.8454572, -0.5735152);
  		  var settings = {
  		        zoom: 14,
  		        center: latlng,
  		        mapTypeControl: false,
  		        scrollwheel: false,
  		        draggable: true,
  		        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
  		        navigationControl: false,
  		        navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
  		        mapTypeId: google.maps.MapTypeId.ROADMAP
  		     };
  			map = new google.maps.Map(document.getElementById("map_canvas"), settings); 
  			
  			google.maps.event.addListener(map, 'center_changed', function (){ _this.mapCenterChanged();});
  			google.maps.event.addListener(map, 'click', function (){ infoBox.close();});
  	    google.maps.event.addListenerOnce(map, 'idle', function(){
			    	_this.mapDidLoad();
  			});
  
  			geocoder = new google.maps.Geocoder();
  			
  			google.maps.event.addListener(map,'zoom_changed',function () {
  				 _this.zoom_changed();
        });
  			
  		    //infoBox
  			
  			var ibOptions = {
  						 content: ''
  						,maxWidth: 0
  						,pixelOffset: new google.maps.Size(-120, -225)
  						,boxStyle: { 
  						  background: "none"
  						  ,opacity: 1
  						  ,width: "240px"
  						  ,height: "180px"
  						 }
  						,closeBoxMargin: "14px 10px 2px 2px"
  						,closeBoxURL: "images/close_white.png"
  						,pane: "floatPane"
  					};
  			infoBox = new InfoBox(ibOptions);	
				
	    /* }); */
	    
	    //simulate fixed for sort-menu
	    $(".sort-menu").width($("#map_list").width() +10);
  		$(window).resize(function(){
  			$(".sort-menu").width($("#map_list").width() +10);
  		});
  		
  		this.getFilters();
  },
  
  getFilters: function(){
    var promises = [];
    var tagsQuery = new Parse.Query('Tag');
    promises.push(tagsQuery.find().then(function(tags){
      _this.setState({tags:tags});
    }));
    var catsQuery = new Parse.Query('Category');
    promises.push(catsQuery.find().then(function(cats){
      _this.setState({categories:cats});
    }));
    Parse.Promise.when(promises).then(function(){
      $('.categories-select').multiselect({
          templates: {
            filter:'<div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div>',
          },
  				enableFiltering: true,
  		      	buttonText: function(options, select) {
  		      		var count = options.length;
  		      		if(count == 0)
  		      		{
  			      		count = 'toutes';
  		      		}
  		      		else if (count > this.numberDisplayed)
  		      		{
  			      		count = 'toutes';
  		      		}
  		      		return 'Categories : '+count;
  		      	}
  		    });
  		$('.tags-select').multiselect({
  		    templates: {
            filter:'<div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div>',
          },
  				enableFiltering: true,
	      	buttonText: function(options, select) {
	      		var count = options.length;
	      		if(count == 0)
	      		{
		      		count = 'tous';
	      		}
	      		return 'Tags : '+count;
	      	}
  		  });
    });
  },
     
  mapDidLoad: function(){
    locationMarker = new google.maps.Marker({
				map: map,
				icon: 'images/location.png',
				title: "Me"
			});
				
		var searchMarker = new google.maps.Marker({
			map: map,
			icon: 'images/marker_search.png',
			title: "Search"
		});
		
		// Get filters
		var cats = GetURLParameter('cats');
		if(cats)
		{
			cats = cats.split('+');
			for (var i = 0; i<cats.length; i++)
			{
				$('.categories-select').multiselect('select',cats[i]);
			}
		}
		var tags = GetURLParameter('tags');
		if(tags)
		{
			tags = tags.split('+');
			for (var i = 0; i<tags.length; i++)
			{
				$('.tags-select').multiselect('select',tags[i]);
			}
		}
		// We're searching something ?
		var q = GetURLParameter('q');
		var qLatLng = GetURLParameter('latlng');
		var qne = GetURLParameter('ne');
		var qsw = GetURLParameter('sw');
		
		if(qLatLng)
		{	
			qLatLng = qLatLng.split(':');
			qLatLng = new google.maps.LatLng(qLatLng[0],qLatLng[1]);
			map.setCenter(qLatLng);
			map.setZoom(15);
			this.loadPlacesHere();
			searchMarker.setPosition(qLatLng);
			return;
		}
		else if(qne && qsw)
		{
			qne = qne.split(':');
			qne = new google.maps.LatLng(qne[0],qne[1]);
			qsw = qsw.split(':');
			qsw = new google.maps.LatLng(qsw[0],qsw[1]);
			var bounds = new google.maps.LatLngBounds();
			bounds.extend(qne);
			bounds.extend(qsw);
			map.fitBounds(bounds);
			this.loadPlacesHere();
			return;
		}
		else if(q == 'geo')
		{
			this.localizeMe();
			return;
		}
		else if(q)
		{
			this.searchPlace(q);
			return;
		}
		this.loadPlacesHere();
  },
  searchPlace: function (q){
    console.log('search : '+q);
  },
  localizeMe: function (){
		    // Try HTML5 geolocation
			  if(navigator.geolocation) {
			  	this.beginRefreshing();
			    navigator.geolocation.getCurrentPosition(function(position) {
			      var pos = new google.maps.LatLng(position.coords.latitude,
			                                       position.coords.longitude);
			
				  locationMarker.setPosition(pos);			
			      map.setCenter(pos);
			      map.setZoom(15);
			      _this.loadPlacesHere();
			      
			    }, function(error) {
					  console.log(error);
			    });
			  } else {
			    // Browser doesn't support Geolocation
			    console.log("geolocation not supported");
			  }
	    },
  loadPlacesHere: function (){
			var bounds = map.getBounds();
			var ne = bounds.getNorthEast();
			var sw = bounds.getSouthWest();
			this.loadPlaces(0,0,ne,sw,[]);
		},
  
  
  //Markers
  removeAllMarkers: function (){
			for (var i = 0; i< markersArray.length; i++)
			{
		     	markersArray[i].setMap(null);
			}
			markersArray.length = 0;
		},
  newMarker: function (place){
				//var address = obj.location.address[0] + obj.location.city;
				var address = place.get("place_address");
				var place_location = place.get("place_location");
				var place_image = "images/bar1.jpg";
				if(place.get("place_image"))
				{
					var place_image = place.get("place_image").url();
				}
				place.place_image = place_image;
				var google_latlng = new google.maps.LatLng(place_location.latitude, place_location.longitude);
				var marker = new google.maps.Marker({
			          map: map,
			          position: google_latlng,
			          icon: _this.getMarkerImgForZoom()
			      });
			    marker.place_id = place.id;
			    markersArray.push(marker);
					
	        google.maps.event.addListener(marker, "click", function() {
	        	_this.updateInfoBox(place);
	        	infoBox.open(map, marker);
	        });
		},
  updateInfoBox: function (place){
			var infoBoxContent ='<i class="arrow-down fa fa-3x fa-heart"></i>'+
	  							'<div onClick="infoBoxClick('+place.id+')" class="panel panel-primary infoBox-panel">'+
	  							  '<div class="panel-heading">'+
	  							  		place.get("place_name")+
	  							  '</div>'+
								  '<div class="panel-body">'+
								    	'<div class="thumbnail img-fill-container" style="background-image: url(\''+place.place_image+'\');">'+
									      /*'<img src="'+place_image+'">'+ */
									    '</div>'+
								  '</div>'+
								'</div>';
      var content = (<PlaceInfoBox place={place}/>);
      infoBoxContent = React.renderComponentToString(content);
			infoBox.setContent(infoBoxContent);
  },
  //Refreshing
  beginRefreshing: function (){
  	$("#refreshControl").addClass("fa-spin");
  },
  endRefreshing: function (){
  	$("#refreshControl").removeClass("fa-spin");
  },
  
  
  mapCenterChanged: function (){
			
		},
  zoom_changed: function (){
			for(var i = 0; i<markersArray.length; i++)
			{
				var marker = markersArray[i];
				marker.setIcon(this.getMarkerImgForZoom());
			}
		},
  getMarkerImgForZoom: function (){
			var marker_img = '';
			if(map.getZoom() > 16)
			{
				marker_img = 'images/marker_red.png';
			}
			else
			{
				marker_img = 'images/marker_pin_red.png';
			}
			return marker_img;
		},
  
  
  /* *********** MAIN LOADPLACES FUNCTION ************ */
  loadPlaces: function (lat,lng, ne, sw, filters){
			this.beginRefreshing();
			var query = new Parse.Query("Place");
			
			query.include("place_category");
						
			// POSITION
			if (lat!=0 && lng!=0)
			{
				var point = new Parse.GeoPoint({latitude: lat, longitude: lng});
				query.near("place_location", point);
			}
			if(ne!=0 && sw!=0)
			{
				var southwest = new Parse.GeoPoint(sw.lat(),sw.lng());
				var northeast = new Parse.GeoPoint(ne.lat(),ne.lng());
				query.withinGeoBox("place_location", southwest, northeast);
			}	
			
			query.limit(300);
			// LAUNCH REQUEST
			query.find({
			  success: function(places) {
			  	
				allPlaces = places;
				_this.applyFilters();
				_this.endRefreshing();
			  },
			  error: function(error) {
			    _this.endRefreshing();
			  }
			});
  },
  applyFilters: function (){
			var places = allPlaces;
			var categories = $('.categories-select').val();
			var tags = $('.tags-select').val();
			visiblePlaces = [];
			this.removeAllMarkers();			    
		  for(var i=0; i<places.length; i++)
		  {
			    var place = places[i];
			    var shouldAdd = true;
			    if(categories)
			    {
				    var place_category = place.get("place_category");
				    if ( categories.indexOf(place_category) == -1)
				    {
					    shouldAdd = false;
				    }
			    }
			    if(tags && shouldAdd)
			    {
				    var place_tags = place.get("place_tags");
				    var containsTag = false;
				    console.log(tags);
				    for(var t = 0; t < tags.length; t++)
				    {
					    var tag = tags[t];
					    
					    if(place_tags.indexOf(tag) != -1)
					    {
						    containsTag = true;
						    break;
					    }
				    }
				    console.log(containsTag);
					shouldAdd = containsTag;
			    }
			    if(shouldAdd)
			    {
			    	visiblePlaces.push(place);
			    	//this.treatPlace(place);
			    	if(!place.get("place_location"))
			    	{
			    		this.foundlatlng(place);
			    	}
  					else
  					{
  						this.newMarker(place);
  					}
				}
			}
			
			if(visiblePlaces.length <= 0)
			{
				var noPlaces = (<div className="alert alert-info" style={{width:'300px',margin:'auto'}}>
    										  <button type="button" className="close" data-dismiss="alert">×</button>
    										  <h4>Aucun lieu trouvé</h4>
    										  <p>Essayez de changer les filtres</p>
    									  </div>);
    		_this.setState({placeListItems:noPlaces});

			}
			else
			{
				this.updatePlaceList();
			}
			this.updateState();
		},
  updateState: function (){
			var url = 'map.php?';
			var bounds = map.getBounds();
			var ne = bounds.getNorthEast();
			var sw = bounds.getSouthWest();
			url += 'ne='+ne.lat()+':'+ne.lng()+'&sw='+sw.lat()+':'+sw.lng();
			
			var categories = $('.categories-select').val();
			if(categories)
			{
				url += '&cats=';
				for (var i = 0; i<categories.length; i++)
				{
					url += categories[i];
					if(i < (categories.length - 1))
					{	url += '+'; }
				}
			}
			
			var tags = $('.tags-select').val();
			if(tags)
			{
				url += '&tags=';
				for (var i = 0; i<tags.length; i++)
				{
					url += tags[i];
					if(i < (tags.length - 1))
					{	url += '+'; }
				}
			}
			//window.history.replaceState({},document.title,url);
		},
  
  updatePlaceList: function (){
			$("#map_list").animate({ scrollTop: 0 }, "500");	
			_this.setState({placeListItems:[]});		
			var places = visiblePlaces;
			var placeListItems = [];
			for(var i = 0; i < places.length; i++)
			{
				if(places[i])
				{
				  placeListItems.push(<PlaceListItem key={i} place={places[i]} />);
				}
			}
			_this.setState({placeListItems:placeListItems});
			this.updateHoverHandler();
		},
  
  updateHoverHandler: function (){
			$("#map_list .place").hover(
				  function() {
				    	var place_id = $( this ).attr("id");
				    	var marker = $.grep(markersArray, function(e){ return e.place_id == place_id; })[0];
				    	marker.setIcon('images/marker_blue.png');
				    	marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
				  }, function() {
				    	var place_id = $( this ).attr("id");
				    	var marker = $.grep(markersArray, function(e){ return e.place_id == place_id; })[0];
				    	marker.setIcon(_this.getMarkerImgForZoom());
				    	marker.setZIndex(google.maps.Marker.MAX_ZINDEX);
				  }	
			);
		},
  
  // if place has no latlng, try address
  foundlatlng: function (place){
			
			if(!place.get("place_location"))
			{
				geocoder.geocode( { 'address': place.get("place_address")}, function(results, status) {
				    if (status == google.maps.GeocoderStatus.OK) {
					     var position = results[0].geometry.location;
					     var point = new Parse.GeoPoint({latitude: position.lat(), longitude: position.lng()});
						 place.set("place_location",point);
						 place.save();
					 }
				  });
			}
		},
  treatPlace: function (place){
			var tags = place.get("place_tags");
			if(tags.length == 1 && tags[0].indexOf(",") > -1)
			{	
				var first = tags[0];
				var values = first.split(", ");
				if(values.length > 1)
				{
					place.set("place_tags",values);
					place.save(null, {
					  success: function(place) {
					    // Execute any logic that should take place after the object is saved.
					    console.log('Saved');
					  },
					  error: function(place, error) {
					    // Execute any logic that should take place if the save fails.
					    // error is a Parse.Error with an error code and description.
					    console.log('Failed ' + error.description);
					  }
					});
				}
			}
			else
			{
			}
		},
  /**
   * @return {object}
   */
  render: function() {
    var categories = this.state.categories;
    var tags = this.state.tags;
    return (
        <div class="map-view">
          <Menu/>
          <div className="map_full_container">
      			
      			<div id="map_container" className="col-md-5 col-sm-6 col-xs-12">
      			
        			<div id="map_canvas">
        			
        			</div>
  
      				<div className="row" id="map-navbar"> 
      				  <div className="btn-group">
      					  <button onClick={this.loadPlacesHere} className="btn btn-primary">
      					  		<span className="hidden-xs">Recharger la zone </span><i id="refreshControl" className="fa fa-refresh"></i>
      					  </button>
      					  <button onClick={this.localizeMe} className="btn btn-primary">
      					  		<span className="hidden-xs">Autour de moi </span><i className="fa fa-location-arrow"></i>
      					  </button>
      					  <a href="#map_list" className="btn btn-default visible-xs">
      					  		Liste 
      					  </a>
      					 </div>
      				</div>
  
      			</div>
      				
      			<div id="map_list" className="col-md-7 col-sm-6 col-xs-12">
  
      					<div className="row sort-menu" id="sort-menu">
  
      						<div className="btn-group">
        						<select className="categories-select" style={{display:'none'}} multiple="multiple">
        							{ categories.map(function(category, i){
                            return (<option key={i}> {category.get('category_name')}  </option>);
                      })}
        						</select>
        						
    
        						<select className="tags-select" style={{display:'none'}} multiple="multiple">
                      { tags.map(function(tag, i){
                            return (<option key={i}> {tag.get('tag_name')}  </option>);
                      })}
        						</select>
      						</div>
      						<div className="pull-right">
      							  <button type="button" className="btn btn-primary" onClick={this.applyFilters}>
      							  	Appliquer <i className="fa fa-refresh"></i>
      							  </button>
      						</div>
      					</div>
  
      
      
      
      				<div className="row-fluid" id="place_list">
      					 {this.state.placeListItems}
      				</div>
      				
      				{ /*<PlaceListPagination page={this.state.page} count={this.state.count} perPage={this.state.count}/>
      				<div id="place_pagination" style={{clear:'both'}}>
      					<ul className="pagination pull-left">
      		      </ul>
      				</div> */}
      				
      				<div className="add-a-bar">
      				  <a href="/newplace.html">Proposer un bar ?</a>
      				</div>
      			</div>
      		</div>
      		
      		
       </div>
    );
  },

});

module.exports = Map;
