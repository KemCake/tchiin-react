/**
 * @jsx React.DOM
 */
 
var React = require('react');
var ReactPropTypes = React.PropTypes;
var Link = require('react-router-component').Link;

var PlaceInfoBox = React.createClass({
  
  /**
   * @return {object}
   */
  render: function() {
      var place = this.props.place;  
      var link = '/place.html?'+place.get('place_slug');
      return (
        <a href={link}>
            <i className="arrow-down fa fa-3x fa-heart"></i>
  					<div className="panel panel-primary infoBox-panel">
    					  <div className="panel-heading">
    					  		{place.get("place_name")}
    					  </div>
    				  <div className="panel-body">
    				    	<div className="thumbnail img-fill-container" style={{backgroundImage: 'url(\''+place.place_image+'\')'}}>
    					    </div>
    				  </div>
    				</div>
    		</a>
    );
  },

});

module.exports = PlaceInfoBox;
