/**
 * @jsx React.DOM
 */
 
var $ = require('jquery');
var Parse = require('parse').Parse;
var React = require('react');
var ReactPropTypes = React.PropTypes;
var Link = require('react-router-component').Link;

var PlaceListPagination = React.createClass({
  
  componentDidMount: function() {
    
  },
  
  _updatePlaceList: function(page) {
    this.props.updateList(page);
  },
  /**
   * @return {object}
   */
  render: function() {
      var count = this.props.count;
      var perPage = this.props.perPage;
      var page = this.props.page;
      var pageLi = [];
      var countPage = Math.ceil( (count/perPage < 1) ? 1 : count/perPage );
  		if(countPage > 1)
  		{
		    var previousPage = page-1 ;
				if(previousPage>=1)
				{
		  		pageLi.push(<li><a onClick={this._updatePlaceList.bind(this,previousPage)}>&#8249;</a></li>);
		    }
		    var begin = 1;
		    var finish = countPage;
		    if( countPage > 10 )
		    {
  		     begin = (page-2 > 0)? page-2 : 1
  		     finish = (page+2 < countPage)? page+2 : countPage 
		    }
		    if(begin > 2)
		    {
			  		pageLi.push(<li><a onClick={this._updatePlaceList.bind(this,1)}>1</a></li>);
			  		pageLi.push(<li className="disabled"><a>..</a></li>);
		    }
		    
		    for (var i=begin; i<= finish; i++)
		    {   
		        var classLi = '';
			  		if(i==page)
			  		{
				  		classLi = 'active';
			  		}
			  		pageLi.push(<li className="classLi"><a onClick={this._updatePlaceList.bind(this,i)}>{i}</a></li>);
		    }	
		    if(finish < countPage-2)
		    {
			  		pageLi.push(<li class="disabled"><a>..</a></li>);
			  		pageLi.push(<li><a onClick={this._updatePlaceList.bind(this,countPage)}>{countPage}</a></li>);
		    }
		    var nextPage = page + 1;
		    if(nextPage <= (i-1))
        {
		  			pageLi.push(<li><a onClick={this._updatePlaceList.bind(this,nextPage)}>&#8250;</a></li>);
        }
	    }
    
    return (
        <div id="place_pagination" style={{clear:'both'}}>
  				<ul className="pagination pull-left">
  				  {pageLi}
  	      </ul>
  			</div>
    );
  },

});

module.exports = PlaceListPagination;
