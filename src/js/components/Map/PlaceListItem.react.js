/**
 * @jsx React.DOM
 */
 
var $ = require('jquery');
var Parse = require('parse').Parse;

var React = require('react');
var ReactPropTypes = React.PropTypes;
var Link = require('react-router-component').Link;

var PlaceListItem = React.createClass({

  getInitialState: function() {
    return {};
  },
  
  componentDidMount: function() {
    
  },
  /**
   * @return {object}
   */
  render: function() {
    var place = this.props.place;

    var category_name = '';
    var place_image = "images/no_photo.png";
		if(place.get("place_image"))
		{
			var place_image = place.get("place_image").url();
		}
		var category_name = "-";
		if(place.get("place_category"))
		{
			var category_name = place.get("place_category").get("category_name");
		}
		var link = '/place.html?'+place.get('place_slug');
    return (
          <div className="col-xl-4 col-md-6 col-sm-12">
            
							<div className="panel panel-primary place" id={place.id}>
							  <div className="panel-heading">
							      <a href={link}>
							  		  {place.get("place_name")}
							  		</a>
							  		<small className="pull-right">
							  			{category_name}
							  		</small>
							  </div>
							  <div className="panel-body">
							  		<div className="place-img-container img-fill-container" style={{backgroundImage: 'url('+place_image+')'}}>
							  		</div>
							  </div>
							  <div className="panel-footer">
							  		<a href={link}>Voir le bar</a>
							  		<div className="panel-share pull-right">
							  			<i className="fa fa-facebook"></i>
							  			<i className="fa fa-twitter"></i>
							  			<i className="fa fa-pinterest"></i>
							  		</div>
							  </div>
							</div>
					</div>
    );
  },

});

module.exports = PlaceListItem;
