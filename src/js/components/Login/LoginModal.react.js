/**
 * Copyright 2013-2014 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @jsx React.DOM
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */

var Parse = require('parse').Parse;
var React = require('react');
var Bootstrap = require('react-bootstrap');
var Router = require('react-router-component');
var Link = Router.Link;



var LoginModal = React.createClass({
  
  getInitialState: function() {
    return {
    };
  },

  componentDidMount: function() {

  },
   
  classicLogin: function ()
  {
  	var username = $(".login-form input[name='username']").val();
  	var password = $(".login-form input[name='password']").val();
  	Parse.User.logIn(username, password, {
  	  success: function(user) {
  	    // Do stuff after successful login.
  	    window.location.href = "/";
  	  },
  	  error: function(user, error) {
  	    // The login failed. Check error to see why.
  	    console.log(error);
  	  }
  	});
  },
  signUp: function ()
  {
  	var username = $(".signup-form input[name='username']").val();
  	var email = $(".signup-form input[name='email']").val();
  	var password = $(".signup-form input[name='password']").val();
  	Parse.User.signUp(username, password,{email:email}, {
  	  success: function(user) {
  	    // Do stuff after successful login.
  	    window.location.href = "/";
  	  },
  	  error: function(user, error) {
  	    // The login failed. Check error to see why.
  	    console.log(error);
  	  }
  	});
  },
  facebookLogin: function ()
  {
  	Parse.FacebookUtils.logIn("user_likes,email", {
  	  success: function(user) {
  	    if (!user.existed()) {
  	      //New user created and logged in with facebook
  	      console.log(user)
  	    } else {
  	      //User logged in
  	      window.location.href = "/";
  	    }
  	  },
  	  error: function(user, error) {
  	    //Cancel or unauthorize
  	  }
  	});
  },

  /**
   * @return {object}
   */
  render: function() {
    var _this = this;
  	return (
          <div className="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
        	  <div className="modal-dialog modal-md">
        	    <div className="modal-content">
        	      <div className="modal-header">
        	        <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	        <h4 className="modal-title" id="loginModalLabel">Se connecter à Tchiin</h4>
        	      </div>
        	      
        	      <div className="modal-body text-center">
        	      	<div className="row">
        					  <button className="btn btn-primary" width="80%" onClick={this.facebookLogin}><span className="fa fa-facebook"></span> Se connecter avec Facebook </button><br/>
                    <button className="btn btn-primary disabled"><span className="fa fa-twitter"></span> Se connecter avec Twitter </button>
        	      	</div>
        	      	<br/>
        	      	<div className="modal-footer text-center">
          	        <div className="row connexion-forms">
            	        <div className="col-md-5 login-form">
              	      	
              	      	<p>Ou de façon classique</p>
                        <div className="row">
                  				<div className="input-group">
                  				  <span className="input-group-addon"><i className="fa fa-user"></i></span>
                  				  <input type="text" className="form-control" name="username" placeholder="Username"/>
                  				</div>
                  				<div className="input-group">
                  				  <span className="input-group-addon"><i className="fa fa-lock"></i></span>
                  				  <input type="password" className="form-control" name="password" placeholder="Password"/>
                  				</div>
                  				<button type="button" className="btn btn-primary" onClick={this.classicLogin}>Se connecter</button>
                        </div>
            	        </div>	
                    
              	        <div className="col-md-5 col-md-offset-2 signup-form">
                          <p>Nouveau sur Tchiin ?</p>
                          <div className="row">
                    				<div className="input-group">
                    				  <span className="input-group-addon"><i className="fa fa-user"></i></span>
                    				  <input type="text" className="form-control" name="username" placeholder="Username"/>
                    				</div>
                    				<div className="input-group">
                    				  <span className="input-group-addon"><i className="fa fa-lock"></i></span>
                    				  <input type="password" className="form-control" name="password" placeholder="Password"/>
                    				</div>
                    				<div className="input-group">
                    				  <span className="input-group-addon"><i className="fa fa-envelope"></i></span>
                    				  <input type="email" className="form-control" name="email" placeholder="Email"/>
                    				</div>
                    				<button type="button" className="btn btn-primary" onClick={this.signUp}>Senregistrer</button>
                          </div>
                        </div>
                      </div>
          	       </div>
        	      </div>
        	      
        	        
        	    </div>
        	  </div>
        	</div>
  	);
  }

});

module.exports = LoginModal;


