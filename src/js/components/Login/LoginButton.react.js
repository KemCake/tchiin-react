/**
 * Copyright 2013-2014 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @jsx React.DOM
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */

var Parse = require('parse').Parse;
var React = require('react');

var LoginButton = React.createClass({
  
  getInitialState: function() {
    return {
    };
  },

  componentDidMount: function() {
    $(function(){
    
    	if(Parse.User.current())
    	{
    		var me = Parse.User.current();
    		me.fetch().then(function(fetchedUser){
    			$("#profile_username").text(me.get("username"));
    			$("#navbar-user-logged").show();
    		}, function(error){
    		    //Handle the error
    		});
    		$("#profile_link").attr("href","profile.html?"+Parse.User.current().get('username'));
    	}	
    	else
    	{
    		$("#navbar-user-not-logged").show();
    	}
    	
    });
  },
  
  logOut: function ()
  {
  	Parse.User.logOut();
  	window.location.href = '/';
  },

  /**
   * @return {object}
   */
  render: function() {
    var _this = this;
  	return (
          <ul className="nav navbar-nav navbar-right">
							<li className="dropdown" id="navbar-user-logged" style={{display:'none'}}>
								<a href="" className="dropdown-toggle" data-toggle="dropdown"><span id="profile_username">Profil</span> <i className="fa fa-angle-down"></i></a>
								<ul className="dropdown-menu">
									<li>
										<a href="profile.html" id="profile_link">Mon Profil</a>
									</li>
									<li>
										<a onClick={this.logOut}>Deconnexion</a>
									</li>
								</ul>
							</li>
						
							<li id="navbar-user-not-logged" style={{display:'none'}}>
								<button className="btn btn-default navbar-btn" data-toggle="modal" data-target="#loginModal">Log In</button>
							</li>
          </ul>
  	);
  }

});

module.exports = LoginButton;


