/**
 * @jsx React.DOM
 */



var React = require('react');
var Link = require('react-router-component').Link;

var About = React.createClass({


  /**
   * @return {object}
   */
  render: function() {
  	return (
        <div className="col-md-8 col-md-offset-2 text-center">
          Empty about page :)
        </div>
        
  	);
  },


});

module.exports = About;


