/**
 * Copyright 2013-2014 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @jsx React.DOM
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */

var Parse = require('parse').Parse;
var React = require('react');

var Router = require('react-router-component');
var Link = Router.Link;

var MainLinks = require('./MainLinks/MainLinks.react');
var LoginButton = require('./Login/LoginButton.react');
var LoginModal = require('./Login/LoginModal.react');
var Menu = React.createClass({
  
  getInitialState: function() {
    return {
    };
  },

  componentDidMount: function() {
    $(function(){
    
    	var input = $("#searchBar").get(0);
    	var options = {
    /*     types: ['(establishement)'], */
        componentRestrictions: {country: "fr"}
       };
    	var autocomplete = new google.maps.places.Autocomplete(input,options);
    	google.maps.event.addListener(autocomplete, 'place_changed', function() {
    	      var place = autocomplete.getPlace();
    	      if (!place.geometry) {
    	        // Inform the user that the place was not found and return.
    	        return;
    	      }
    	
    	      var latlng = place.geometry.location.lat()+':'+place.geometry.location.lng();
    		  $("#searchLatLng").val(latlng);
        });
    });
    

  },

  /**
   * @return {object}
   */
  render: function() {
    var _this = this;
  	return (
  	    <div className="nav-container">
          <div id="navigation" className="navbar navbar-default navbar-fixed-top" role="navigation">
        		<div className="navbar-inner">
        			<div className="navbar-header">
        				<button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        					<span className="sr-only">Toggle navigation</span>
        					<i className="fa fa-bars fa-2x"></i>
        				</button>
        				<a id="brand" className="navbar-brand" href="/"> <img src="images/title_white.png"/> </a>
        			</div>
        			
        			<div className="navbar-collapse collapse">
        		    <ul className="nav navbar-nav navbar-left">
        						<li className="dropdown">
        							<a className="dropdown-toggle" data-toggle="dropdown">
        							  Envi de ... <i className="fa fa-angle-down"></i>
        						  </a>
        							<MainLinks type="list"/>
        						</li>
        
        						<li>
        							<a href="/map.html" className="int-collapse-menu">Carte</a>
        						</li>
        						<li>
        							<Link href="/#/place/10" className="int-collapse-menu">A propos</Link>
        						</li>
        				</ul>
        					
        				<LoginButton/>
                      
                <div className="col-sm-4 col-md-5 navbar-right">
  				        <form className="navbar-form" role="search" method="get" action="map.html">
  					        <div className="input-group input-group-md" style={{width:'100%',paddingTop:'5px'}}>
  					            <input type="text" className="form-control" id="searchBar" placeholder="Rechercher quelque chose ?" name="s"/>
  					            <div className="input-group-btn">
  					                <button className="btn btn-default" type="submit"><span className="fa fa-search"></span></button>
  					            </div>
  					        </div>
  					        <input type="hidden" id="searchLatLng" name="latlng"/>
  				        </form>
  			        </div>
      			       
              </div>
        			
        		</div>
        		
        	</div>
        	
        	<LoginModal/>
        	
        </div> 
  	);
  }

});

module.exports = Menu;


