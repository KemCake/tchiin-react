/**
 * @jsx React.DOM
 */
 
var $ = require('jquery');
var Parse = require('parse').Parse;
var React = require('react');


var Menu = require('../Menu.react');
var Link = require('react-router-component').Link;

var Profile = React.createClass({

  getInitialState: function() {
    return {profile:null};
  },
  
  componentDidMount: function() {
    $(function(){
      $(".has-tooltip").tooltip();
      $(".has-popover").popover();
     });
     this.setState({profile:Parse.User.current()});
     this.fetchCurrentProfile();
  },
  
  fetchCurrentProfile: function(){
    var _this = this;
    Parse.User.current().fetch().then(function(user){
     _this.setState({profile:user});
    });
  },
  /**
   * @return {object}
   */
  render: function() {
    var profile = this.state.profile
    if(!profile)
      return (<p> loading...</p>);
    return (
      <div className="profile-view">
        <Menu/>
        
        <div className="profile-full-container container-fluid">
			
      			<div className="col-xs-12 col-sm-6 col-md-4 pull-left text-center profile-left-container">
      				<div className="profile-sidebar text-left panel panel-primary">
      					<div className="panel-heading">
      			        {profile.get('username')}
      			      	</div>
      			      	<div className="panel-body">
      						<img className="thumbnail w100"/>
      						<h2>
      							{profile.get('username')}<br/>
      						</h2>
      						<p className="place-infos">
      						</p>
      			      	</div>
      				</div>
      				
      				<div className="profile-sidebar text-left panel panel-primary">
      					<div className="panel-heading">
      			      			Partage
      			      	</div>
      			      	<div className="panel-body">
      						<p>
      							<div className="btn-group btn-group-justified">
      								<a className="btn btn-primary">Facebook</a>
      								<a className="btn btn-info">Twitter</a>
      								<a className="btn btn-danger">Pinterest</a>
      							</div>
      						</p>
      			    </div>
      				</div>
      			</div>
      			
      			
            <div className="col-xs-12 col-sm-6 col-md-8 pull-right text-center">
      				<div className="profile-container panel panel-primary text-left">
      					<div className="panel-body">
      						<div className="col-md-6">
      							<h4> Favoris </h4>
      							<p>
      							  mes lieux favoris
      							</p>
      						</div>
      						<div className="col-md-6">
      							<h4> Check-ins </h4>
      							<p>
      								derniers checkins
      							</p>
      						</div>
      					</div>			
      				</div>

      				

      				<div className="profile-container panel panel-primary">
		
      				</div>

      				
      			</div>
      			
      		</div>{/* full container */}
      </div>
    );
  },

});

module.exports = Profile;
