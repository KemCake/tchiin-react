/**
 * Copyright 2013-2014 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @jsx React.DOM
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */

var $ = require('jquery');

var Parse = require('parse').Parse;

var React = require('react');
var MainLinks = require('../MainLinks/MainLinks.react');
var Header = React.createClass({


  componentDidMount: function() {

  },

  /**
   * @return {object}
   */
  render: function() {
  	return (
      <section id="home">
  			<div id="one-parallax" className="parallax" style={{backgroundImage: "url('images/bar2.jpg')"}} data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
  				<div className="parallax-overlay">
  					<div className="section-content">
  						<div className="container text-center">
  	
  							<div className="parallax-content">
  							
  							  <div className="section-title text-center">
									  <h2 className="item_left">On sort boire un truck ?</h2>
                  </div>
                  
                  <div className="row">
  									<div className="col-md-offset-2 col-md-1 col-xs-offset-0 col-xs-2">
  										<a href="map.html?q=geo" className="btn btn-primary btn-lg has-tooltip">
  											<i className="fa fa-location-arrow "></i> <span className="hidden-xs">Autour de moi</span>
  										</a>
  									</div>
  									<div className="col-md-offset-1 col-md-6 col-xs-offset-0 col-xs-10">
    									<form className="" role="search" method="get" action="map.html">
    									      <div className="input-group input-group-lg">
    										      <input type="text" className="form-control" name="q" id="homeSearchBar" placeholder="Ou chercher quelque chose" />
    										      <input type="hidden" id="homeSearchLatLng" name="latlng"/>
    										      <span className="input-group-btn">
    										        <button className="btn btn-default" type="submit"><span className="fa fa-search"></span></button>
    										      </span>
    										    </div>
    									 </form>
  									</div>
  								</div>
								
                  <MainLinks type="icons"/>
								
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  		</section>

  	);
  },


});

module.exports = Header;


