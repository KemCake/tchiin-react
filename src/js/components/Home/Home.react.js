/**
 * @jsx React.DOM
 */
 
var $ = require('jquery');

require('modernizr');
require("jqparallax");
require("jqcountTo");
require("jqappear");
require("jqpiechart");
require("jqcycle");
require("jqmaximage");
require("jqisotope");
require("skrollr");
require("flexslider");
require("jqhoverdir");
require("jqvalidate");
require("retina");
require("theme");

var Parse = require('parse').Parse;

var React = require('react');
var ReactPropTypes = React.PropTypes;
var Menu = require('../Menu.react');

var Link = require('react-router-component').Link;
var Header = require('./Header.react');

var Home = React.createClass({

  getInitialState: function() {
    return {};
  },
  
  componentDidMount: function() {
    
    $(function(){
		  getAdvices();
		  getNumbers();
			var input = $("#homeSearchBar").get(0);
			var options = {
      componentRestrictions: {country: "fr"}
      };
			var autocomplete = new google.maps.places.Autocomplete(input,options);
			
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
			      var place = autocomplete.getPlace();
			      if (!place.geometry) {
			        // Inform the user that the place was not found and return.
			        return;
			      }
			
			      var latlng = place.geometry.location.lat()+':'+place.geometry.location.lng();
				  $("#homeSearchLatLng").val(latlng);
		    });
		});
		
		function getAdvices()
		{
    		var query = new Parse.Query("Advice");
    		query.ascending('order');
  			query.find({
  			  success: function(advices) {
  			      advices.forEach(function(advice) {
  			          var img = advice.get('image');
  			          img = img? img.url():'';//img.url();
                  var item = $('<div class="col-sm-6 col-md-4 advice">'+
                    					    '<div class="thumbnail">'+
                    					      '<img src="'+img+'" alt="...">'+
                    					      '<div class="caption">'+
                    					        '<h3>'+advice.get('title')+'</h3>'+
                    					        '<p>'+advice.get('description')+'</p>'+
                    					        '<p><a href="'+advice.get('link')+'" class="btn btn-primary" role="button">Voir</a></p>'+
                    					      '</div>'+
                    					    '</div>'/
                    					  '</div>');
            		 	$('.home-advices').append(item);
              });
              $.getScript('//cdn.jsdelivr.net/isotope/1.5.25/jquery.isotope.min.js',function(){
                  /* activate jquery isotope */
                  $('.home-advices').imagesLoaded( function(){
                    $('.home-advices').isotope({
                      itemSelector : '.advice'
                    });
                  });
               });
  			  },
  			  error: function(error) {
              console.log(error);
  			  }
  			});
		}
		
		function getNumbers()
		{
    		var numberUsers = new Parse.Query("User");
  			numberUsers.count({
          success: function(count) {
              $('.numbers-users').attr('data-to', count);
              launchCounter($('.numbers-users'));
          }
        });
        
        var numberPlaces = new Parse.Query("Place");
  			numberPlaces.count({
          success: function(count) {
              $('.numbers-places').attr('data-to', count);
              launchCounter($('.numbers-places'));
          }
        });
		}
		
		function launchCounter(e)
		{
				var el = $(e);
				el.appear(function() {
					el.countTo({});
				}, {
					accX : 0,
					accY : -150
				});
		}
    
  },
  /**
   * @return {object}
   */
  render: function() {
    
    return (
      <div className="home-view">
          <Header/>
          
      		<Menu />
      		
          <section id="first" className="section-content">
      			<div className="container">
      				<div className="section-title text-center">
      					<div>
      						<span className="line big"></span>
      						<span>Voyons voir</span>
      						<span className="line big"></span>
      					</div>
      					<h1 className="item_right">Envi d autre chose?</h1>
      					<div>
      						<span className="line"></span>
      						<span>Quoi de beau ce soir ?</span>
      						<span className="line"></span>
      					</div>
      				</div>
      				<br/><br/>
      				<div className="container-fluid">
      					<div className="row home-advices">                  
      					</div>
      				</div>
      			</div>
      		</section>
      		
      		
      		<div id="two-parallax" className="parallax" style={{backgroundImage: "url('images/bar1.jpg')"}} data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
      			<div className="parallax-overlay">
      				<div className="section-content">
      					<div className="container text-center">
      						<h1>Ca tchiin a max ! </h1>
      						<p className="lead">
      							Voici ce que vous avez accompli
      						</p>
      
      						<div className="parallax-content">
      							<div className="row text-center number-counters">
      								<div className="col-md-3 col-sm-6">
      									<div className="counters-item element-line">
      										<i className="fa fa-group fa-4x"></i>
      										<strong className="numbers-users">0</strong>
      										<p className="lead">
      											Personnes inscrites
      										</p>
      									</div>
      								</div>
      								<div className="col-md-3 col-sm-6">
      									<div className="counters-item element-line">
      										<i className="fa fa-flag fa-4x"></i>
      										<strong className="numbers-places">0</strong>
      										<p className="lead">
      											Bars référencés
      										</p>
      									</div>
      								</div>
      								<div className="col-md-3 col-sm-6">
      									<div className="counters-item element-line">
      										<i className="fa fa-glass fa-4x"></i>
      										<strong data-to="15200">0</strong>
      										<p className="lead">
      											Verres ont fait tchiin
      										</p>
      									</div>
      								</div>
      								<div className="col-md-3 col-sm-6">
      									<div className="counters-item element-line">
      										<i className="fa fa-rocket fa-4x"></i>
      										<strong data-to="540">0</strong>
      										<p className="lead">
      											Invitations envoyées
      										</p>
      									</div>
      								</div>
      							</div>
      						</div>
      
      					</div>
      				</div>
      			</div>
      		</div>

      </div>
    );
  },

});

module.exports = Home;
