/**
 * Copyright 2013-2014 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @jsx React.DOM
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */

var $ = require('jquery');
var Parse = require('parse').Parse;
var React = require('react');
var MainLink = require('./MainLink.react');

var MainLinks = React.createClass({

  getInitialState:function (){
    return {allLinks:null};
  },
  componentDidMount: function() {
    this.getMainLinks();
  },
  
  getMainLinks: function() {
  		var query = new Parse.Query("MainLink");
  		_this = this;
			query.find().then(function(links) {
			   _this.setState({allLinks:links});
			});
	},

  /**
   * @return {object}
   */
  render: function() {
    var allLinks = this.state.allLinks;
    var links = [];

    if (allLinks) {
      for (var key in allLinks) {
        links.push(<MainLink key={key} type={this.props.type} link={allLinks[key]}/>);
      }
    }

    if(this.props.type == 'list'){
      return (
        <ul className="dropdown-menu navbarMainLinks">
      		{links}					   
        </ul>
      );
    }
    else {
    	return (
        <div className="row" id="mainLinks">
          {links}
  			</div>
    	);
    }
  },


});

module.exports = MainLinks;


