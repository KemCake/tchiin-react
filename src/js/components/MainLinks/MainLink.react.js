/**
 * @jsx React.DOM
 */

var $ = require('jquery');
var Parse = require('parse').Parse;
var React = require('react');

var MainLink = React.createClass({

  /**
   * @return {object}
   */
  render: function() {
    var link = this.props.link;
    if(this.props.type == "list"){
      return (
        <li>
          <a href={link.get('link')}>
            {link.get('title')}
           </a>
        </li>
      );
    }
    
  	return (
        <div className="col-md-3 col-sm-3 col-xs-6">
					<div className="element-line">
						<div className="hi-icon-container">
							<div className="hi-icon-effect-1">
								<a href={link.get('link')} className=""> 
								  <i className={'hi-icon fa fa-'+link.get('icon')+' fa-2x'}></i> 
								</a>
							</div>
							<span>{link.get('title')}</span>
							<p className="lead hidden-xs">
                {link.get('subtitle')}
							</p>
						</div>
					</div>
				</div>
  	);
  },


});

module.exports = MainLink;


