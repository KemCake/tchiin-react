/**
 * @jsx React.DOM
 */
 

var Parse = require('parse').Parse;
var React = require('react');

var Menu = require('../Menu.react');

var NewPlace = React.createClass({

  getInitialState: function() {
    return {};
  },
  
  componentDidMount: function() {
    
  },
  
  /**
   * @return {object}
   */
  render: function() {
    var type = "add";
    if(this.props.place)
      type = "modify";
    return (
      <div className="new-place-view">
        <Menu/>
        add or modify a place ?
        <h3>{type}</h3>
      </div>
    );
  },

});

module.exports = NewPlace;
