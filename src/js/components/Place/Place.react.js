/**
 * @jsx React.DOM
 */
 
var $ = require('jquery');

var Parse = require('parse').Parse;
var React = require('react');
var yelp = require("yelp").createClient({
  consumer_key: "YV7I7KFJPl86oGHU7tZwYQ", 
  consumer_secret: "gHEHHS1dBWy_9-1FKAUBT0Bkpn8",
  token: "ET1dGEixSoowYWltVmSUA7WIGW72xn8u",
  token_secret: "8kMxIogy7Twb5sIPdCbUdVQFBX4"
});

var Menu = require('../Menu.react');
var Link = require('react-router-component').Link;
var PlaceStore = require('../../stores/PlaceStore');
var YelpReviews = require('./YelpReviews.react');

var openToday;
var Place = React.createClass({

  getInitialState: function() {
    return {place:null, schedule_html:'<h1>hyo</h1>'};
  },
  
  componentDidMount: function() {
    var lat = 45;
		var lng = -0.5;
		var map;
    $(function(){
      		
			// load Google Map          
	      
     });
     PlaceStore.getBySlug(this.props.placeName, this.getPlace);
  },
  componentDidUpdate: function() {
    $(".has-tooltip").tooltip();
    $(".has-popover").popover();
    var place = this.state.place;
    if(place) {
        var geopoint = place.get('place_location');
        var latlng = new google.maps.LatLng(geopoint.latitude, geopoint.longitude);
	      	
		    var settings = {
		        zoom: 17,
		        center: latlng, 
		        mapTypeControl: false,
		        scrollwheel: false,
		        draggable: false,
		        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
		        navigationControl: false,
		        navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		     };
		    
         map = new google.maps.Map(document.getElementById("place-map"), settings);
         var marker = new google.maps.Marker({
			          map: map,
			          position: latlng,
			          icon:'images/marker_red.png'
			      });
		}
  },
  getPlace: function(place){
    this.setState({place:place});
    this.parseSchedule(place);
  },
  parseSchedule: function(place){
    /* SCHEDULE PARSING */
  	var daysArray = ["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"];
  	var todayDay = new Date().getDay() || 7;
  	openToday = true;
  	var todayHours = ' ';
  	var place_schedule_html = '<p>';
  	var place_schedule = place.get('place_schedule');
  	if(place_schedule)
  	{
  		for(var i=1; i<7; i++) {

  		  var value = place_schedule[i];
  			dayString = daysArray[i - 1]; 
  			place_schedule_html += dayString+' : ';
  			var scheduleDay = i;
  			var daySchedule = value;
  			
  			if(!daySchedule.length)
  			{
  				place_schedule_html += '<span class="pull-right label label-danger">Fermé</span>';
  				if(todayDay == scheduleDay)
  				{
  					openToday = false;
  				}
  			}
  			else
  			{
  				for (var key in daySchedule){
  				  var dayHours = daySchedule[key];
  					var start = dayHours[0];
  					var end = dayHours[1];
  					place_schedule_html += '<span class="pull-right">'+start+' à '+end+'</span>';
  					if(todayDay == scheduleDay)
  					{
  						todayHours+= start+' à '+end+' ';
  					}
  				}
  			}
  			place_schedule_html += '<br/>';
      }
    }
    place_schedule_html += '</p>';
  	this.setState({schedule_html:place_schedule_html});
  },
  /**
   * @return {object}
   */
  render: function() {
    var place = this.state.place;
    if(!place)
      return (<p>Loading...</p>);
    var place_image 
    if(place.get('place_image')) {
      place_image = place.get('place_image').url();
    } else {
      place_image = 'images/bar1.jpg';
    }
    var geopoint = place.get('place_location');
    var place_tags = place.get('place_tags');
    var tags = [];
    for (var key in place_tags) {
      tags.push(<span className="label label-primary" key={key}> {place_tags[key]} </span>);
    }
    
    return (
      <div className="map-view">
        <Menu/>
        
        <div className="place-full-container container-fluid">
			
      			<div className="col-xs-12 col-sm-6 col-md-4 pull-left text-center place-left-container">
      				<div className="place-sidebar text-left panel panel-primary">
      					<div className="panel-heading">
      			        {place.get('place_name')}
      			      	</div>
      			      	<div className="panel-body">
      						<img className="thumbnail w100" src={place_image}/>
      						<h2>
      							{place.get('place_name')}<br/>
      							<small>{place.get('place_address')}</small>
      						</h2>
      						<p className="place-infos">
      							<i className="fa fa-phone"> </i> 
      							<a href="tel:00000000">{place.get('place_phone')}</a><br/>
      							<i className="fa fa-globe"> </i> 
      							<a href={place.get('place_website')}>{place.get('place_website')}</a><br/>
      						   
      						</p>
      			      	</div>
      				</div>
      				
      				<div className="place-sidebar text-left panel panel-primary">
      					<div className="panel-heading">
      			      			Partage
      			      	</div>
      			      	<div className="panel-body">
      						<p>
      							<div className="btn-group btn-group-justified">
      								<a className="btn btn-primary">Facebook</a>
      								<a className="btn btn-info">Twitter</a>
      								<a className="btn btn-danger">Pinterest</a>
      							</div>
      							 <a href={"/newplace.html?"+place.id}>Ajouter des informations ?</a>
      						</p>
      			      	</div>
      				</div>
      			</div>

      			

      			<div className="col-xs-12 col-sm-6 col-md-8 pull-right text-center">
      				<div className="place-container panel panel-primary text-left">
      					<div className="panel-body">
      						<div className="col-md-6">
      							<h4> Horaires </h4>
      							<p>
      							{ openToday ?
      							  <a className="has-popover btn btn-success btn-sm" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-content={this.state.schedule_html}>Ouvert aujourdhui de </a>
      							  :
      							  <a className="has-popover btn btn-danger btn-sm" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-content={this.state.schedule_html}>Fermé aujourdhui</a>
      						  }
      							</p>
      							<h4> Happy hours </h4>
      							<p>
      								{place.get('place_happy_hours')}
      							</p>
      							<h4> Tags </h4>
      							<p>
      								{tags}
      							</p>
      						</div>
      						<div className="col-md-6">
      							<h4> Description </h4>
      							<p>
      								{place.get('place_description')}
      							</p>
      						</div>
      					</div>			
      				</div>

      				

      				<div className="place-container panel panel-primary">
      					<div id="place-map" className="col-md-8">
      					</div>	
      					<div id="place-map-sidebar" className="col-md-4 text-left">
      						<h4>address</h4>
      						<p className="place-infos">
      							<i className="fa fa-flag"> </i> district<br/> 
      						</p>
      						<div className="btn-group btn-group-justified">
      						  <a href="#" className="btn btn-info">Google</a>
      						  <a href="#" className="btn btn-info">Itinéraire</a>
      						</div>
      						<br/><br/>
      						<a href={'map.html?latlng='+geopoint.latitude+':'+geopoint.longitude}>Autres bars autour ?</a>
      					</div>		
      				</div>


      				<YelpReviews place={place}/>
      				
      			</div>
      						
      		</div>
      </div>
    );
  },

});

module.exports = Place;
