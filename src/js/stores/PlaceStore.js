var Parse = require('parse').Parse;
var Place = Parse.Object.extend('Place', {}, {
  
  getById: function(objectId, callback) {
    var query = new Parse.Query(Place);
    query.get(objectId, {
      success: function(place) {
        callback(place);
      },
      error: function(obj, err) {
        console.error('getById() error', obj, err);
      }
    });
  },
  
  getBySlug: function(slug, callback) {
    var query = new Parse.Query(Place);
    query.equalTo("place_slug",slug);
    query.find({
      success: function(places) {
        var place = places[0];
        callback(place);
      },
      error: function(obj, err) {
        console.error('getBySlug() error', obj, err);
      }
    });
  },
  
  getAll: function(callback) {
    var query = new Parse.Query(Place);
    query.descending("createdAt");
    query.find({
      success: function(places) {
        callback(places);
      },
      error: function(obj, err) {
        console.error('getAll() error', obj, err);
      }
    });
  }
});

module.exports = Place;