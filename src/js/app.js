/**
 * @jsx React.DOM
 */

var $ = require('jquery');

var React = require('react');

var Parse = require('parse').Parse;
var ParseConfig = require('./stores/ParseConfig.json');
Parse.initialize(ParseConfig.APP_ID, ParseConfig.JS_KEY);

(function(d, s, id){
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement(s); js.id = id;
 js.src = "//connect.facebook.net/en_US/all.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
	  Parse.FacebookUtils.init({
	    appId      : '1492569200963902', // Facebook App ID
	    channelUrl : '//www.tchi.in/', // Channel File
	    status     : true, // check login status
	    cookie     : true, // enable cookies to allow Parse to access the session
	    xfbml      : true  // parse XFBML
	  });
};


	  
	  
var App = require('./components/App.react');
React.renderComponent(
  <App />,
  document.body
);



require('bootstrap');
require('modernizr');
require("sticky");
require("jqparallax");
require("jqcountTo");
require("jqappear");
require("jqpiechart");
require("jqcycle");
require("jqmaximage");
require("jqisotope");
require("skrollr");
require("flexslider");
require("jqhoverdir");
require("jqvalidate");
require("theme");
require("retina");
	

$(function(){
		$(".has-tooltip").tooltip();
		$(".has-popover").popover();
});

window.GetURLParameter = function(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return decodeURIComponent(sParameterName[1]);
        }
    }
};
